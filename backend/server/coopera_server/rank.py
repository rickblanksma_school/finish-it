# from tinydb import TinyDB, where
# from tinyrecord import transaction
import dataset

from coopera_server.app import socketio
from coopera_server.utils import now

db = dataset.connect('sqlite:///database/sqlite.db')
ranks = db['ranks']
# ranks = TinyDB('db.json').table('ranks')


class RankList(object):

    # def __init__(self):
    #     self.ranks = []

    def get_team(self, players):
        # return  sorted([player.name for player in players])
        return ' '.join(sorted([player.name for player in players]))

    def try_update(self, level, players):
        '''
        Update rank if the new level is higher.
        Return information about rank.
        '''
        team = self.get_team(players)
        # result = ranks.search(where('team') == team)
        # import IPython;IPython.embed()
        # with transaction(ranks) as tr:
        #     tr.upsert({
        #         'date': now(),
        #         'level': level,
        #         'team': team
        #     })
        previous_highest = ranks.find_one(team=team)
        if not previous_highest or previous_highest['level'] < level:
            new_rank = {
                'date': now(),
                'level': level,
                'team': team
            }
            ranks.upsert(new_rank, 'team')
            new_highest = True

            socketio.emit(
                'update_rank_list',
                self.to_json(),
                room='>rank_list')
        else:
            new_highest = False

        return {
            'new_highest': new_highest,
            'previous_highest': previous_highest
        }

    # def get_highest_score(self, players):
    #     result = ranks.search(where('team') == self.get_team(players))
    #     return result[0] if result else None

    def to_json(self):
        # return sorted(ranks.all(), key=lambda x: x['level'], reverse=True)
        # import IPython;IPython.embed()
        return list(ranks.find(order_by='-level'))
