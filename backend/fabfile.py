import os
import re
import sys
from os.path import join
from importlib import import_module
from contextlib import contextmanager

from fabric.api import env, cd, prefix, sudo, hide, task
from fabric.contrib.files import exists, upload_template
from fabric.contrib.project import rsync_project


###########
# Configs #
###########

env.use_ssh_config = True

conf = {}
if sys.argv[0].split(os.sep)[-1] in ("fab", "fab-script.py"):
    # Ensure we import settings from the current dir
    try:
        conf = import_module("server.settings.local_settings").FABRIC
        try:
            conf["HOSTS"][0]
        except (KeyError, ValueError):
            raise ImportError
    except (ImportError, AttributeError):
        print("Aborting, no hosts defined.")
        exit()

env.hosts = conf.get("HOSTS", [""])
env.secret_key = conf.get("SECRET_KEY", "")

env.proj_name = 'coopera'
env.proj_user = 'coopera'
env.proj_path = '/home/%s/%s' % (env.proj_user, env.proj_name)
env.client_dist_path = join(env.proj_path, 'client', 'dist')
env.server_path = join(env.proj_path, 'server')
env.venv_path = join(env.proj_path, 'server', 'env')
env.domains = conf.get("DOMAINS", [conf.get("LIVE_HOSTNAME", env.hosts[0])])
env.certificate_filename = conf.get("CERTIFICATE_FILENAME", env.domains[0])
env.domains_nginx = " ".join(env.domains)
env.domains_regex = "|".join(env.domains)
env.log_folder = '/home/%s' % env.proj_user
env.matomo_domain = conf.get("MATOMO_DOMAIN", env.domains[0])
env.client_domain = conf.get("CLIENT_DOMAIN", env.domains[0])

templates = {
    "nginx_ssl": {
        "local_path": "deploy/nginx_ssl_params.template",
        "remote_path": "/etc/nginx/%(proj_name)s_ssl_params",
        "reload_command": "nginx -t && service nginx restart",
    },
    "nginx": {
        "local_path": "deploy/nginx.conf.template",
        "remote_path": "/etc/nginx/sites-enabled/%(proj_name)s.conf",
        "reload_command": "nginx -t && service nginx restart",
    },
    "supervisor": {
        "local_path": "deploy/supervisor.conf.template",
        # Debian
        "remote_path": "/etc/supervisor/conf.d/%(proj_name)s.conf",
        # Arch Linux
        # "remote_path": "/etc/supervisor.d/%(proj_name)s.ini",
        "reload_command": "supervisorctl update %(proj_name)s",
    },
    "settings": {
        "local_path": "deploy/local_settings.py.template",
        "remote_path": "%(proj_path)s/server/settings/local_settings.py",
    },
    "matomo": {
        "local_path": "deploy/nginx_matomo.conf.template",
        "remote_path": "/etc/nginx/sites-enabled/matomo.conf",
        "reload_command": "nginx -t && service nginx restart",
    },
}


############
# Commands #
############

def run(command):
    sudo(command, user=env.proj_user, group=env.proj_user)


@contextmanager
def virtualenv():
    """
    Runs commands within the project's virtualenv.
    """
    with cd(env.venv_path):
        with prefix("source %s/bin/activate" % env.venv_path):
            yield


@contextmanager
def server():
    """
    Runs commands within the project's directory.
    """
    with virtualenv():
        with cd(env.server_path):
            yield


def rsync_upload():
    """
    Uploads the project with rsync excluding some files and folders.
    """
    excludes = [
        "*.pyc", "*.pyo", "*.db", ".DS_Store", ".coverage", "env",
        "local_settings.py", "/static", "/.git", "/.hg", 'node_modules',
        '__pycache__'
    ]
    local_dir = os.getcwd() + os.sep
    return rsync_project(
        remote_dir=env.proj_path, local_dir=local_dir, exclude=excludes,
        extra_opts='--rsync-path "sudo -u %s rsync"' % env.proj_user)


def get_templates():
    """
    Returns each of the templates with env vars injected.
    """
    injected = {}
    for name, data in templates.items():
        injected[name] = dict([(k, v % env) for k, v in data.items()])
    return injected


def upload_template_and_reload(name):
    """
    Uploads a template only if it has changed, and if so, reload the
    related service.
    """
    template = get_templates()[name]
    local_path = join('server', template["local_path"])
    if not os.path.exists(local_path):
        project_root = os.path.dirname(os.path.abspath(__file__))
        local_path = os.path.join(project_root, local_path)
    remote_path = template["remote_path"]
    reload_command = template.get("reload_command")
    owner = template.get("owner")
    mode = template.get("mode")
    remote_data = ""
    if exists(remote_path):
        with hide("stdout"):
            remote_data = sudo("cat %s" % remote_path)
    with open(local_path, "r") as f:
        local_data = f.read()
        # Escape all non-string-formatting-placeholder occurrences of '%':
        local_data = re.sub(r"%(?!\(\w+\)s)", "%%", local_data)
        local_data %= env
    clean = lambda s: s.replace("\n", "").replace("\r", "").strip()
    if clean(remote_data) == clean(local_data):
        return
    upload_template(local_path, remote_path, env, use_sudo=True, backup=False)
    if owner:
        sudo("chown %s %s" % (owner, remote_path))
    if mode:
        sudo("chmod %s %s" % (mode, remote_path))
    if reload_command:
        sudo(reload_command)


def install_deps():
    with server():
        run('pip install -r requirements.txt')


#########
# Tasks #
#########


@task
def install():
    # Requires: python-virtualenv supervisor

    sudo('useradd -m %s' % env.proj_user)
    run("mkdir -p %s" % env.proj_path)
    rsync_upload()
    run('virtualenv %s' % env.venv_path)
    install_deps()


@task
def deploy():
    rsync_upload()
    for name in get_templates():
        upload_template_and_reload(name)
    install_deps()
    restart()


@task
def restart():
    sudo("supervisorctl restart %s" % env.proj_name)
