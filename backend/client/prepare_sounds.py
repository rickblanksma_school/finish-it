#!/bin/env python

import os
from os.path import join
import subprocess

base_path = os.path.dirname(os.path.abspath(__file__))
sounds_dir = join(base_path, 'audio_project', 'tracks')
nosilence_dir = join(sounds_dir, 'nosilence')

# rename files to remove track number
for f in os.listdir(sounds_dir):
    if '_' in f:
        os.rename(join(sounds_dir, f), join(sounds_dir, f.partition('_')[2]))

# remove silence
os.makedirs(nosilence_dir, exist_ok=True)
for f in os.listdir(sounds_dir):
    if os.path.isfile(join(sounds_dir, f)):
        subprocess.run(
            'sox {original} {nosilence} silence -l 1 0.1 0% -1 1.0 0%'.format(
                original=join(sounds_dir, f),
                nosilence=join(nosilence_dir, f)
            ),
            shell=True
        )

# create single file and json
os.chdir('static')
subprocess.run(
    '../node_modules/.bin/audiosprite '
    '--format howler2 '
    '--output sounds '
    '--export ogg,webm '
    '--gap 0 '
    '--channels 2 '
    '--path static '
    '%s/*' % nosilence_dir,
    shell=True
)

# os.rmdir(nosilence_dir)
