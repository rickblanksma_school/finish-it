import Vue from 'vue'
import Router from 'vue-router'
import GameRoom from '@/components/GameRoom'
import RoomsList from '@/components/RoomsList'
import RankList from '@/components/RankList'
import AboutPage from '@/components/AboutPage'
import HelpPage from '@/components/HelpPage'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect: { name: 'HelpPage' }
    },
    {
      path: '/rooms',
      name: 'RoomsList',
      component: RoomsList
    },
    {
      path: '/help',
      name: 'HelpPage',
      component: HelpPage
    },
    {
      path: '/rank',
      name: 'RankList',
      component: RankList
    },
    {
      path: '/about',
      name: 'AboutPage',
      component: AboutPage
    },
    {
      path: '/room/:name',
      name: 'GameRoom',
      component: GameRoom
    }
  ],
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
})
