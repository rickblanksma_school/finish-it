// Get a value from localStorage without crashing
export function safeGetLS (key) {
  try {
    return localStorage.getItem(key)
  } catch (e) {
    return null
  }
}

// Set a value to localStorage without crashing
export function safeSetLS (key, value) {
  try {
    localStorage.setItem(key, value)
    return true
  } catch (e) {
    return false
  }
}
