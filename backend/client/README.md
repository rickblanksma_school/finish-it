# Coopera Client

This client is configured to be autobuilt and hosted in GitLab Pages. So creating a modified client that works with the same server should be as easy as forking this project, modifying the code, setting `assetsPublicPath` (see below) and enabling GitLab Pages for your repository.

If you do so, please send me a note! =D

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For deployment, don't forget to set `assetsPublicPath` in `config/index.js`. The current config is for a deployment in the root domain, like `example.org`. For a `coopera` subpath, like `example.org/coopera`, set it to `/coopera/`.

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
