<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top justify-content-between">
	<a class="navbar-brand" href="/"><img src="{{ asset('img/logos/finish-it-logo.svg') }}" width="50" height="50" /> <h2>Home</h2></a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarFinishIt" aria-controls="navbarFinishIt" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="nav-title text-center w-100">
		Make-a-Thon 2019
	</div>
	<div class="collapse navbar-collapse justify-content-end" id="navbarFinishIt">
		<ul class="navbar-nav">
			<li class="nav-item">
				<a class="nav-link" href="cam">Cam</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="gallery">Gallery</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="info">Info</a>
			</li>
		</ul>
	</div>
</nav>