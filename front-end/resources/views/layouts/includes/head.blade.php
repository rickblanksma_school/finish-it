<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="MakeAThon MBO Utrecht FinishIt Finish-It">
<meta name="description" content="Een website waar je in real life een autotje kan besturen.">
<meta name="author" content="Finish-It (Pepijn, Lars, Tim, Ceyhun, Rick)">

<title>Finish-IT - @yield('title')</title>

<!-- Favicon -->
<link href="{{ asset('img/logos/finish-it-logo.png') }}" rel="shortcut icon" type="image/ico" />

<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" />

<!-- Styles -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
<link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" />

<!-- Scripts -->
<script src="{{ asset('js/jquery/jquery-3.3.1.js') }}"></script>
<script src="{{ asset('js/jquery/jquery.easing.1.3.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap/bootstrap.min.js') }}"></script>

<noscript>Sorry, your browser does not support JavaScript!</noscript>

<!--[if lt IE 9]><!-->
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<!--<![endif]-->