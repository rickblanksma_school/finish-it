<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('layouts.includes.head')
    </head>
    <header>
        @include('layouts.includes.nav')
    </header>
    <body>
        <main>
            @yield('content')
        </main>
        @include('layouts.includes.footer')
    </body>
</html>

<script type="text/javascript">
    var height = $(window).height() - $('nav').outerHeight( true );
    $('header').css('min-height', $('nav').outerHeight( true ));
    $('.fullscreen').css('height', height);
</script>