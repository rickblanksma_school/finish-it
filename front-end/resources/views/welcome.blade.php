@extends('layouts.main')
@section('title', 'Welcome')

@section('content')
<div class="flex-top position-ref my-5">
    <div class="content">
        <div class="title">
            <img src="{{ asset('img/logos/finish-it-logo.svg') }}" height="100" width="100" />
            Finish-It
        </div>
        <div class="subtitle animated bounceInOut">
            The power to drive
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-7">
                    <div class="card">
                        <div class="card-header text-left">
                            <h2 class="text-white">Webcam</h2>
                        </div>
                        <div class="card-img-top">
                            <img src="https://webcamfeed-pepijn.pitunnel.com" alt="Webcam" class="webcam" />
                        </div>
                        <div class="card-footer">
                            <button id="left" class="btn btn-primary"><span class="arrow-left"><i class="fas fa-arrow-left fa-2x"></i></span></button>
                            <button id="up" class="btn btn-primary"><span class="arrow-up"><i class="fas fa-arrow-up fa-2x"></i></span></button>
                            <button id="right" class="btn btn-primary"><span class="arrow-right"><i class="fas fa-arrow-right fa-2x"></i></span></button>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-5">
                    <div class="card">
                        <div class="card-header text-left">
                            <h2 class="text-white">Chat</h2>
                        </div>
                        <div class="card-img-top">
                            <iframe class="chat" src='https://minnit.chat/FinishIt?embed' width='1000' height='500' style='border:none;' allowTransparency='true'></iframe>
                        </div>
                        <div class="card-footer">
                            <a class="link" href='https://minnit.chat/FinishIt' target='_blank'>Free HTML5 Chatroom powered by Minnit Chat</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    // $('.card').mousedown(function(evt){
    //     console.log('test');
    //     console.log(evt);
    //     $('.card').mouseup(function(){
    //         return false;
    //     });
    //     $(window).keydown(function(evt) {
    //         if (evt.which === 37 || evt.which === 65) { // arrow left
    //             console.log('arrow left');
    //         }
    //         if (evt.which === 38 || evt.which === 87) { // arrow up
    //             console.log('arrow up');
    //         }
    //         if (evt.which === 39 || evt.which === 68) { // arrow right
    //             console.log('arrow right');
    //         }
    //     });
    // });
        
    $('button#left').click(function() {
        $.ajax({
            url: '/drivingCar',
            data: $(this).serialize(),
            type: 'POST',
            success: function(response) {
                console.log(response);
            },
            error: function(error) {
                console.log(error);
            }
        });
    });
    $('button#up').click(function() {
        $.ajax({
            url: '/drivingCar',
            data: $(this).serialize(),
            type: 'POST',
            success: function(response) {
                console.log(response);
            },
            error: function(error) {
                console.log(error);
            }
        });
    });
    $('button#right').click(function() {
        $.ajax({
            url: '/drivingCar',
            data: $(this).serialize(),
            type: 'POST',
            success: function(response) {
                console.log(response);
            },
            error: function(error) {
                console.log(error);
            }
        });
    });
</script>
@endsection