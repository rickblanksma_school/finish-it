@extends('layouts.main')
@section('title', 'Welcome')

@section('content')
<div class="flex-top position-ref my-5">
    <div class="content">
    	<a href="/">
	        <div class="title">
	            <img src="{{ asset('img/logos/finish-it-logo.svg') }}" height="100" width="100" />
	            Finish-It
	        </div>
	        <div class="subtitle">
	            The power to drive
	        </div>
	    </a>
        <hr>
        <div class="containe">
        	<div class="row">
        		<div class="col-12 text-center">
		        	<p>De <a href="https://makeathon-utrecht.nl/evenement/">Make-a-thon</a> is een evenement welke is georganiseerd door het MBO Utrecht. Er zijn meer dan 20 groepen om binnen 48 uur een project opleveren welke iets toevoegd aan het bedrijfsleven.<br>
		        	We zijn een op afstand bestuurbare auto aan het maken die door iedereen bestuurd kan worden.<br><br>
		        	Dit vercht veel samenwerking, omdat het autootje de kant op gaat waar de meeste mensen hem heen willen hebben. <br>
		        	Deze samenwerking staat parallel aan samenwerking binnen het bedrijfsleven.<br>
		        	We willen mensen laten ervaren dat de keuzes die je maakt niet altijd realiteit kunnen worden.<br>
		        	Samenwerking verhoogt de kans op succes!<br>
		        	Voor een voordelige teambuilding, of binnen een korte tijdsspanne is dit het beste concept om het functioneren (van de samenwerking) binnen uw team te verbeteren.<br>
		        	Waar wacht je nog op, Finish It!</p>
		        	<hr>
		        	
		        	<div class="col-sm-3 mx-auto">
		        	<p>
		        		Met dank aan:
		        		<ul class="list-group d-inline">
		        			<li class="list-group-item"><a href="http://rickblanksma.nl">Rick Blanksma</a></li>
		        			<li class="list-group-item"><a href="https://pepijnvanderstap.nl">Pepijn van der Stap</a></li>
		        			<li class="list-group-item">Tim Kwaijtaal</li>
		        			<li class="list-group-item">Lars van Doremalen</li>
		        		</ul>
		        	</p>
		        </div>
	        	</div>
	        </div>
        </div>
    </div>
</div>
@endsection