@extends('layouts.main')
@section('title', 'Welcome')

@section('content')
<div class="container">
	<div class="row">
	  <div class="col-md-12 d-flex justify-content-center my-5">
	    <button type="button" class="btn btn-outline-black waves-effect filter" data-rel="all">Alles</button>
	    <button type="button" class="btn btn-outline-black waves-effect filter" data-rel="1">Staand</button>
	    <button type="button" class="btn btn-outline-black waves-effect filter" data-rel="2">Liggend</button>
	  </div>
		<div class="col-12">
			<div class="gallery" id="gallery">
				<div class="row">
					<?php $files = glob("/home/pepijxh324/domains/finishit.nl/public_html/img/gallery/*.{jpg,png,gif}", GLOB_BRACE);
					foreach($files as $file) {
						$file_path = explode('/home/pepijxh324/domains/finishit.nl/public_html/', $file)[1];
						$file_name = str_replace('.jpg', '', explode('/home/pepijxh324/domains/finishit.nl/public_html/img/gallery/', $file)[1]);
						list($width, $height, $type, $attr) = getimagesize($file);
						?>
						<div class="col-12 col-md-3" data-toggle="modal" data-target="#galleryModal">
							<div class="mb-3 pics animation all <?php if( $width - $height > 0 ){ echo 2; } else { echo 1; } ?>">
								<img class="img-fluid" src="<?php echo $file_path; ?>" data-src="<?php echo $file_path; ?>" alt="<?php echo $file_name; ?>">
							</div>
						</div>
				<?php } ?>
				</div>
				<div class="modal fade" id="galleryModal" tabindex="-1" role="dialog" aria-labelledby="galleryModalLabel" aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
						<div class="modal-content">
							<div id="carouselGalleryControls" class="carousel slide" data-ride="carousel">
							  <div class="carousel-inner">
							  	<?php
							  	$i = 0;
							  	foreach($files as $file) {
							  			$file_path = explode('/home/pepijxh324/domains/finishit.nl/public_html/', $file)[1];
							  			$file_name = str_replace('.jpg', '', explode('/home/pepijxh324/domains/finishit.nl/public_html/img/gallery/', $file)[1]);
							  	?>
							    <div class="carousel-item <?php if($i === 0){ ?> active <?php } ?>">
							      <img class="d-block" src="<?php echo $file_path; ?>" data-src="<?php echo $file_path; ?>" alt="<?php echo $file_name; ?>">
							    </div>
								<?php $i++; } ?>
							  </div>
							  <a class="carousel-control-prev" href="#carouselGalleryControls" role="button" data-slide="prev">
							    <span class="carousel-control-prev-icon" aria-hidden="true"><i class="fas fa-chevron-left fa-3x"></i></span>
							    <span class="sr-only">Previous</span>
							  </a>
							  <a class="carousel-control-next" href="#carouselGalleryControls" role="button" data-slide="next">
							    <span class="carousel-control-next-icon" aria-hidden="true"><i class="fas fa-chevron-right fa-3x"></i></span>
							    <span class="sr-only">Next</span>
							  </a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function init() {
    let imgDefer = document.getElementsByTagName('img');
    for (let i=0; i<imgDefer.length; i++) {
        if(imgDefer[i].getAttribute('data-src')) {
            imgDefer[i].setAttribute('src',imgDefer[i].getAttribute('data-src'));
        } } }
	window.onload = init;

	$(function() {
		var selectedClass = "";
		$(".filter").click(function(){
			selectedClass = $(this).data("rel");
			$("#gallery").fadeTo(100, 0.1);
			$("#gallery .all").not("."+selectedClass).fadeOut().removeClass('animation');
			setTimeout(function() {
				$('.all').closest('.col-12.col-md-3').addClass('d-none');
				$("."+selectedClass).closest('.col-12.col-md-3').toggleClass('d-none');
				$("."+selectedClass).fadeIn().addClass('animation');
				$("#gallery").fadeTo(300, 1);
			}, 300);
		});

		$('.col-3').click(function(){
			target = $(this).data('target');
			$(target).modal();
		});
	});
</script>
@endsection